# Build Stage
FROM node:12-alpine AS build

ARG BuildPath=/usr/src/app
ARG NPM_TOKEN_4873
ARG NPM_TOKEN_4874

WORKDIR $BuildPath

COPY .npmrc.template .npmrc
## Install devdependencies first for building time to cache
COPY ["./package*.json", "./yarn.lock", "./"]
RUN yarn install

COPY . .
RUN yarn run build

# Serve Stage
FROM nginx:stable-alpine

ARG BuildPath=/usr/src/app

COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build $BuildPath/build /var/www
EXPOSE 8080
