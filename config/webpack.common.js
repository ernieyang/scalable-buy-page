const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");
const dotenv = require("dotenv");

const MiniCssExtractPlugin = require("mini-css-extract-plugin");

dotenv.config();

module.exports = (argvs) => {
  return {
    entry: "./src/index.tsx",
    module: {
      rules: [
        {
          test: /\.(ts|js)x?$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader",
          },
        },
        {
          test: /\.css$/,
          use: [MiniCssExtractPlugin.loader, "css-loader", "postcss-loader"],
        },
        {
          test: /\.(png|jpe?g|gif|svg|webp)$/i,
          use: [
            {
              loader: "url-loader",
              options: {
                limit: 10 * 1024, //bytes
                name: "[hash:7].[ext]",
                esModule: false,
              },
            },
          ],
        },
      ],
    },
    resolve: {
      extensions: [".tsx", ".ts", ".js", ".jsx"],
      alias: {},
    },
    plugins: [
      new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
        minify: false,
        template: "public/index.html",
        filename: "index.html",
        inject: "body",
        scriptLoading: "defer",
      }),
      //new ForkTsCheckerWebpackPlugin({
      //  async: false,
      //  eslint: {
      //    files: "./src/**/*",
      //  },
      //}),
      new MiniCssExtractPlugin({
        filename: "css/[name].[contenthash:8].css",
        chunkFilename: "css/[name].[contenthash:8].css",
      }),
    ],
    output: {
      filename: "[name].[contenthash].js",
      path: path.resolve(__dirname, "../build"),
    },
  };
};
