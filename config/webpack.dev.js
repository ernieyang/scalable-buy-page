const path = require("path");
const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin");
const webpack = require("webpack");
const { merge } = require("webpack-merge");
const common = require("./webpack.common.js");

module.exports = (argvs) =>
  merge(common(argvs), {
    mode: "development",
    devtool: "inline-source-map",
    cache: {
      type: "filesystem",
      cacheDirectory: path.resolve(__dirname, "../node_modules/.temp_cache"),
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new ReactRefreshWebpackPlugin(),
    ],
    devServer: {
      hot: true,
      host: "0.0.0.0",
      overlay: true,
      disableHostCheck: true,
      port: 8081,
      historyApiFallback: {
        rewrites: [{ from: /./, to: "/index.html" }],
      },
    },
  });
