import cls from "classnames";
import { useState, memo, useEffect } from 'react';
import { useRecoilValue } from "recoil";
import { FormikValues, useFormik } from "formik";
import { object, string } from "yup";
import Modal from "./components/Modal";
import Button from "./components/Button";
import InputText from "./components/InputText";

import BraintreeProvider, {
  BraintreeFieldEnum,
  useBraintreeContext,
  useBraintreeValidation,
} from "./components/BrainTree/context";

import { pageInfoState } from "./atoms/page";
import { productvariantState, quantityState } from "./atoms/checkout";

import BraintreeField from "./components/BrainTree/BraintreeField";

import { US_STATES_HASH_MAP } from "./constants/state";

import { convertMoney } from "./utils";

const StateOption = () => (
  <>
    <option disabled value="">
      State
    </option>
    {Object.entries(US_STATES_HASH_MAP).map(([code, name]) => (
      <option key={code} value={code}>
        {name}
      </option>
    ))}
  </>
);

enum CheckoutStatus {
  FORM,
  DONE,
}

interface CheckoutProps {
  open: boolean;
  onClose: () => void;
}

const checkoutFormSchema = object().shape({
  name: string().required("Name is required."),
  email: string().email().required("Email is required"),
  addr: string().required("Street address is required."),
  country: string().required("Country is required."),
  city: string().required("City is required."),
  state: string().when("country", (country: string) =>
    country === "US" ? string().required("State is required.") : string()
  ),
  zip: string().required("Zip code is required."),
});

const inputProps = (name: string, formik: FormikValues) => ({
  name,
  onChange: formik.handleChange,
  onBlur: formik.handleBlur,
  value: formik.values[name],
  error: formik.touched[name] && !!formik.errors[name],
});

const Checkout: React.FC = () => {
  const [checkoutStatus, setCheckoutStatus] = useState<CheckoutStatus>(CheckoutStatus.FORM);
  const page = useRecoilValue(pageInfoState);
  const productVariant = useRecoilValue(productvariantState);
  const quantity = useRecoilValue(quantityState);

  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      addr: "",
      apt: "",
      city: "",
      state: "",
      zip: "",
      country: "US",
    },
    validateOnChange: false,
    validationSchema: checkoutFormSchema,
    onSubmit: async (values) => {
      console.log(values);
    },
  });

  const productTotal = quantity * productVariant.price;
  const tax = 0;
  const shippingFee = 0;

  const {
    state: { hostedInstance },
  } = useBraintreeContext();

  const [getValidation, setValidation] = useBraintreeValidation();

  const width = { width: "600px" };

  const Form = () => (
      <>
        <div className="px-10 py-6 overflow-auto flex-1">
          <div className="mb-10">
            <div className="bg-white p-4 flex mb-4">
              <div className="" style={{ width: "80px" }}>
                <img src="https://ae01.alicdn.com/kf/Hbe41c06d0a5045dc9d314b38427cf25b2/COLMI-P8-Plus-1-69-inch-2021-Smart-Watch-Men-Full-Touch-Fitness-Tracker-IP67-waterproof.jpg_Q90.jpg_.webp" />
              </div>
              <div className="flex-1 ml-4">
                <div className="mb-1">{page?.product?.name || ""}</div>
                <div className="text-sm">
                  ${convertMoney(productVariant.price)}
                </div>
                <div className="mb-1">
                  {(productVariant.selectedOptions || []).map((so) => {
                    const variantOption = page.product.variantOptions.find(
                      (vo) => vo.id === so.id
                    );
                    return (
                      <span className='text-darkGray-1 text-sm mr-1' key={`${variantOption.id}-${so.value}`}>
                        {
                          variantOption.options.find(
                            (o) => o.value === so.value
                          ).displayName
                        }
                      </span>
                    );
                  })}
                </div>
                <div className="text-sm">Quantity: {quantity}</div>
              </div>
            </div>
            <div className="ml-auto" style={{ width: "252px" }}>
              <div className="flex justify-between">
                <div className="text-darkGray-1">Subtotal</div>
                <div className="text-darkGray-1">
                  ${convertMoney(productTotal)}
                </div>
              </div>
              <div className="flex justify-between">
                <div className="text-darkGray-1">Tax</div>
                <div className="text-darkGray-1">${convertMoney(tax)}</div>
              </div>
              <div className="flex justify-between">
                <div className="text-darkGray-1">Shipping fee</div>
                <div className="text-darkGray-1">
                  ${convertMoney(shippingFee)}
                </div>
              </div>
              <hr className="my-2" />
              <div className="flex justify-between">
                <div className="text-darkBlue-1">Order total</div>
                <div className="text-darkBlue-1">
                  ${convertMoney(productTotal + tax + shippingFee)}
                </div>
              </div>
            </div>
          </div>
          <div className="mb-10">
            <h2 className="text-2xl text-darkGray-1 mb-2">Shipping Info</h2>
            <div>
              <div>
                <InputText
                  {...inputProps("name", formik)}
                  fullWidth
                  placeholder="Name"
                />
              </div>
              <div>
                <InputText
                  {...inputProps("email", formik)}
                  fullWidth
                  placeholder="Email"
                />
              </div>
              <div className="flex">
                <div style={{ flex: "1 1 70%" }}>
                  <InputText
                    {...inputProps("addr", formik)}
                    fullWidth
                    placeholder="Street Address"
                  />
                </div>
                <div className="ml-4" style={{ flex: "1 1 30%" }}>
                  <InputText
                    {...inputProps("apt", formik)}
                    placeholder="Apt, Suite"
                  />
                </div>
              </div>
              <div>
                <InputText
                  {...inputProps("city", formik)}
                  fullWidth
                  placeholder="City"
                />
              </div>
              <div className="flex">
                <div style={{ flex: "1 1 70%" }}>
                  {formik.values["country"] === "US" ? (
                    <div>
                      <select
                        {...inputProps("state", formik)}
                        className={cls(
                          "border border-gray-300 my-2 p-2 w-full",
                          {
                            "border-red-400": formik.errors["state"],
                          }
                        )}
                      >
                        <StateOption />
                      </select>
                    </div>
                  ) : (
                    <InputText
                      {...inputProps("state", formik)}
                      fullWidth
                      placeholder="State"
                    />
                  )}
                </div>
                <div className="ml-4" style={{ flex: "1 1 30%" }}>
                  <InputText
                    {...inputProps("zip", formik)}
                    fullWidth
                    placeholder="Zip"
                  />
                </div>
              </div>
              <div>
                <select
                  name="country"
                  value={formik.values["country"]}
                  className="border border-gray-300 my-2 p-2 w-full"
                  onChange={(e: React.ChangeEvent<HTMLSelectElement>) => {
                    formik.setFieldValue("state", "");
                    //queryTax({
                    //  country: e.target.value,
                    //  state: formik.values['state'],
                    //  zip: formik.values['zip'],
                    //});
                    //onUpdateShippingFee(e.target.value);
                    formik.handleChange(e);
                  }}
                >
                  <option value="US">United State</option>
                  <option value="GB">United Kingdom</option>
                </select>
              </div>
            </div>
          </div>
          <div>
            <h2 className="text-2xl text-darkGray-1">Payment Method</h2>
            <BraintreeField
              id="braintree_number"
              field="number"
              placeholder="Debit or Credit Card Number"
              onValid={setValidation}
              isValid={getValidation(BraintreeFieldEnum.NUMBER)}
            />
            <div className="flex">
              <div style={{ flex: "1 1 50%" }}>
                <BraintreeField
                  id="braintree_expiration"
                  field="expirationDate"
                  placeholder="MM/YY"
                  onValid={setValidation}
                  isValid={getValidation(BraintreeFieldEnum.EXPIREDDATE)}
                />
              </div>
              <div className="ml-4" style={{ flex: "1 1 50%" }}>
                <BraintreeField
                  id="braintree_cvv"
                  field={BraintreeFieldEnum.CVV}
                  placeholder="CVV"
                  onValid={setValidation}
                  isValid={getValidation(BraintreeFieldEnum.CVV)}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="px-10 pt-6 pb-8 bg-lightGray-1 flex-shrink-0">
          <Button onClick={formik.handleSubmit}>
            Place Order: ${convertMoney(productTotal + tax + shippingFee)}
          </Button>
          <div className="mt-4 text-center">
            By placing your order, you agree to our terms of service.
          </div>
        </div>
      </>
  )

  const Done = () => (
    <div>
      <div>We’ve received your order.</div>
      <div>Please check email for order confirmation.</div>
    </div>
  )


  return (
    <div
      className="fixed top-0 right-0 bottom-0 bg-lightGray-1"
      style={width}
    >
      <div className="relative h-screen flex flex-col">
        <div className={cls({'hidden': checkoutStatus !== CheckoutStatus.FORM})}>
          {Form()}
        </div>
        <div className={cls({'hidden': checkoutStatus !== CheckoutStatus.DONE})}>
          {Done()}
        </div>
      </div>
    </div>
  );
};

const WithBrainTree: React.FC<CheckoutProps> = ({ open, onClose }) => (
  <Modal keepMounted open={open} onClose={onClose}>
    <BraintreeProvider token="sandbox_gp9d7sfq_ks2tkp7ndpkcccnw">
      <Checkout />
    </BraintreeProvider>
  </Modal>
);

export default WithBrainTree;
