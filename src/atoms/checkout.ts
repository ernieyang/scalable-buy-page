import { find, prop, pipe, mergeAll, equals } from "ramda";
import { atom, selector } from "recoil";
import { pageInfoState } from "./page";
interface ICheckoutOption {
  [key: string]: string;
}

export const checkoutInfoState = atom<{
  data: ICheckoutOption;
  quantity: number;
}>({
  key: "CheckoutInfoState",
  default: {
    data: {},
    quantity: 1,
  },
});

export const selectedOptionState = selector<ICheckoutOption>({
  key: "CheckoutOptionState",
  get: ({ get }) => get(checkoutInfoState).data,
  set: ({ get, set }, newValue) =>
    set(checkoutInfoState, {
      ...get(checkoutInfoState),
      data: newValue as ICheckoutOption,
    }),
});

export const quantityState = selector<number>({
  key: "QuantityState",
  get: ({ get }) => get(checkoutInfoState).quantity,
  set: ({ get, set }, newValue) =>
    set(checkoutInfoState, {
      ...get(checkoutInfoState),
      quantity: newValue as number,
    }),
});

export const productvariantState = selector<number>({
  key: "ProductvariantState",
  get: ({ get }) => {
    const page = get(pageInfoState);
    if (!page) {
      return {};
    }
    const variants = page.product.variants;
    const selectedOptions = get(selectedOptionState);

    const variant = find(
      pipe(
        prop('selectedOptions'),
        (sos) => sos.map(so => ({ [so.id]: so.value })),
        mergeAll,
        equals(selectedOptions),
      )
    )(variants);

    return variant || {};

  },
});
