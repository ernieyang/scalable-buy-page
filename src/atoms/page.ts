import { atom } from "recoil";

export const pageInfoState = atom<unknown | null>({
  key: "PageInfoState",
  default: null,
});
