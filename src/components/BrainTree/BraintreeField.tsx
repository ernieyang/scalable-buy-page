// @ts-nocheck
import { useEffect, memo, useMemo } from "react";
import cls from "classnames";
import { useBraintreeContext } from "./context";

const BrainTreeInject: React.FC = ({
  id,
  isValid,
  field,
  placeholder,
  //onFocus,
  //onBlur,
  onValid,
  ...rest
}) => {
  const {
    state: { client, hostedInstance },
    registerFields,
  } = useBraintreeContext();

  const handlers = useMemo(() =>( {
    onFocus: () => {},
    onBlur: () => {},
    onValid,
  }), [onValid]);

  useEffect(() => {
    registerFields(field, {
      selector: `#${id}`,
      placeholder,
      handlers,
    });
  }, [id, field, placeholder, handlers, registerFields]);


  const handleClick = () => {
    hostedInstance.focus(field);
  };

  return (
    <div
      className={cls(
        "border border-gray-300 p-2 my-2 bg-white",
        { "w-full": true },
        {
          "border-red-400": !isValid,
        }
      )}
      id={id}
      style={{ height: 42 }}
      onClick={handleClick}
      {...rest}
    />
  );
};

//interface BrainTreeFieldProps {
//  [x: string]: any;
//  id: string;
//  field: string;
//  placeholder: string;
//}

//const BrainTreeField: React.FC<BrainTreeFieldProps> = ({
//  id,
//  field,
//  onValid,
//  placeholder,
//  isValid,
//}) => {
//  console.log(12);
//  return <InputText fullWidth error={!isValid} ref={BrainTreeInject} />;
//};



export default memo(BrainTreeInject);
