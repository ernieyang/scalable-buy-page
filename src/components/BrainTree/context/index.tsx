// @ts-nocheck
import React, {
  createContext,
  useState,
  useContext,
  useEffect,
  useCallback,
  useRef,
  useMemo,
} from "react";
import { client, hostedFields, Client, HostedFields } from "braintree-web";

function useIsMountedRef() {
  const isMountedRef = useRef<boolean>(true);

  useEffect(() => {
    isMountedRef.current = true;
    return () => {
      isMountedRef.current = false;
    };
  });

  return isMountedRef;
}

export enum BraintreeFieldEnum {
  NUMBER = "number",
  CVV = "cvv",
  EXPIREDDATE = "expirationDate",
}

interface BraintreeContextState {
  client: Client | undefined;
  hostedInstance: HostedFields | undefined;
}

interface BraintreeContextProviderStore {
  state: BraintreeContextState;
}

const BraintreeContext = createContext({} as BraintreeContextProviderStore);

// Hook
export const usePrevious = (value) => {
  // The ref object is a generic container whose current property is mutable ...
  // ... and can hold any value, similar to an instance property on a class
  const ref = useRef();

  // Store current value in ref
  useEffect(() => {
    ref.current = value;
  }, [value]); // Only re-run if value changes

  // Return previous value (happens before update in useEffect above)
  return ref.current;
};

const BraintreeContextProvider: React.FC<{ token: string }> = ({
  token,
  children,
}) => {
  const [clientInstance, setClientInstance] = useState<Client>();
  const [hostedInstance, setHostedInstance] = useState<HostedFields>();
  const preInstance = usePrevious(clientInstance);
  const preHostedInstance = usePrevious(hostedInstance);
  const isMountedRef = useIsMountedRef();

  const fields = useRef({});

  const registerFields = useCallback((field, options) => {
    fields.current = { ...fields.current, [field]: options };
  },  []);

  useEffect(() => {
    /*
       in general, It should wait until fields create(may it will have side effect in child did mount)
       but currently, child component has no side effect. and parent has side effect(fetch)
       so we can assume that is not gonna to create bugs
    */
    (async () => {
      const instance = await client.create({ authorization: token });
      if (isMountedRef.current) {
        setClientInstance(instance);
      }
    })();
  }, [token, isMountedRef]);

  useEffect(() => {
    if (preInstance === undefined && clientInstance) {
      (async () => {
        const hostedInstance = await hostedFields.create({
          client: clientInstance,
          styles: {
            input: {
              "font-size": "1rem",
            },
          },
          fields: Object.entries(fields.current).reduce(
            (s, [k, { selector, placeholder }]) => ({
              ...s,
              [k]: { selector, placeholder },
            }),
            {}
          ),
        });

        setHostedInstance(hostedInstance);
      })();
    }
  }, [preInstance, clientInstance]);

  useEffect(() => {
    if (preHostedInstance === undefined && hostedInstance) {
      hostedInstance.on("focus", function (event) {
        fields.current[event.emittedBy].handlers.onFocus();
      });
      hostedInstance.on("blur", function (event) {
        fields.current[event.emittedBy].handlers.onBlur();
        const valid = event.fields[event.emittedBy].isValid;

        fields.current[event.emittedBy].handlers.onValid(
          event.emittedBy,
          valid
        );
      });
      hostedInstance.on("validityChange", function (event) {
        const valid = event.fields[event.emittedBy].isValid;

        if (valid) {
          fields.current[event.emittedBy].handlers.onValid(
            event.emittedBy,
            valid
          );
        }
      });
    }
  }, [preHostedInstance, hostedInstance]);


  const value = useMemo(() =>({
    state: {
      client: clientInstance,
      hostedInstance,
    },
    registerFields,
  }), [clientInstance, hostedInstance, registerFields])

  return (
    <BraintreeContext.Provider value={value}>
      {children}
    </BraintreeContext.Provider>
  );
};

export const useBraintreeValidation = () => {
  const [isNumberValidation, setIsNumberValidation] = useState(true);
  const [isExpiredDateValidation, setIsExpiredDateValidation] = useState(true);
  const [isCVVValidation, setIsCVVValidation] = useState(true);

  const getValidation = useCallback(
    (field: BraintreeFieldEnum): boolean => {
      return {
        [BraintreeFieldEnum.NUMBER]: isNumberValidation,
        [BraintreeFieldEnum.CVV]: isCVVValidation,
        [BraintreeFieldEnum.EXPIREDDATE]: isExpiredDateValidation,
      }[field];
    },
    [isNumberValidation, isCVVValidation, isExpiredDateValidation]
  );

  const setValidation = useCallback(
    (field: BraintreeFieldEnum, isValid: boolean) => {
      ({
        [BraintreeFieldEnum.NUMBER]: setIsNumberValidation,
        [BraintreeFieldEnum.CVV]: setIsCVVValidation,
        [BraintreeFieldEnum.EXPIREDDATE]: setIsExpiredDateValidation,
      }[field](isValid));
    },
    []
  );

  return [getValidation, setValidation] as [
    typeof getValidation,
    typeof setValidation
  ];
};

export const useCreditCardContext = (hostedInstance) => {
  const [creditCardType, setCreditCardType] = useState("");
  const creditCardTypeRef = useRef(creditCardType);
  const prevHostedInstance = usePrevious(hostedInstance);

  useEffect(() => {
    if (prevHostedInstance === undefined && hostedInstance) {
      hostedInstance.on("cardTypeChange", (e: any) => {
        if (e.cards.length === 1 && !creditCardTypeRef.current) {
          setCreditCardType(e.cards[0].type);
        } else if (e.cards.length > 1 && creditCardTypeRef.current) {
          setCreditCardType("");
        }
      });
    }
  }, [prevHostedInstance, hostedInstance]);

  return creditCardType;
};

export default BraintreeContextProvider;
export const BraintreeContextConsumer = BraintreeContext.Consumer;
export const useBraintreeContext = () => useContext(BraintreeContext);
