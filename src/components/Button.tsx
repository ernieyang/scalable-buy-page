interface ButtonProps {
  onClick: () => void;
}
const Button: React.FC<ButtonProps> = ({ onClick, children }) => {
  return (
    <button
      onClick={onClick}
      className="text-white bg-darkBlue-1 text-xl py-4 px-3 w-full font-bold"
    >
      {children}
    </button>
  );
};

export default Button;
