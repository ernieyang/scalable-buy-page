import { useState } from "react";
import cls from "classnames";

interface CollapseProps {
  title: string;
}

const Collapse: React.FC<CollapseProps> = ({ title, children }) => {
  const [open, setOpen] = useState(true);
  return (
    <div className="bg-white">
      <div
        className="text-darkBlue-1 cursor-pointer p-6"
        onClick={() => {
          setOpen(!open);
        }}
      >
        {title}
      </div>
      <div className={cls("text-darkGray-1 px-6 pb-12", { hidden: !open })}>
        {children}
      </div>
    </div>
  );
};

export default Collapse;
