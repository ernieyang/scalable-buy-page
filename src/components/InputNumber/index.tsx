import { useCallback } from "react";

interface InputNumberProps {
  max?: number;
  min?: number;
  value: number;
  onChange: (currentValue: number, event?: React.SyntheticEvent) => void;
}

const InputNumber: React.FC<InputNumberProps> = ({
  min = -Infinity,
  max = Infinity,
  value,
  onChange,
}) => {
  const handleValue = useCallback(
    (currentValue: number, event?: React.SyntheticEvent) => {
      //console.log(currentValue);
      //if (currentValue !== value) {

      onChange(currentValue, event);
      //}
    },
    [max, min]
  );

  const getSafeValue = useCallback(
    (value) => {
      if (!Number.isNaN(value)) {
        if (+value > max) {
          value = max;
        }
        if (+value < min) {
          value = min;
        }
      } else {
        value = "";
      }
      return value.toString();
    },
    [max, min]
  );

  const handlePlus = useCallback(
    (event: React.SyntheticEvent) => {
      const val = +(value || 0);
      handleValue(getSafeValue(val + 1), event);
    },
    [getSafeValue, handleValue, value]
  );
  const handleMinus = useCallback(
    (event: React.SyntheticEvent) => {
      const val = +(value || 0);
      handleValue(getSafeValue(val - 1), event);
    },
    [getSafeValue, handleValue, value]
  );

  const handleChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = event.target;
      if (!/^-?(?:\d+)?(\.)?(\d+)*$/.test(value) && value !== "") {
        return;
      }
      handleValue(getSafeValue(parseFloat(value)), event);
    },
    [getSafeValue, handleValue]
  );

  const handleBlur = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const targetValue = Number.parseFloat(event.target?.value);
      handleValue(getSafeValue(targetValue), event);
    },
    [getSafeValue, handleValue]
  );

  return (
    <div className="flex">
      <button
        className="text-center border cursor-pointer bg-white"
        style={{ width: "50px", height: "50px", lineHeight: "50px" }}
        onClick={handleMinus}
      >
        -
      </button>
      <input
        className="border text-center"
        type="text"
        name="name"
        pattern="[0-9]"
        title="Title"
        value={value}
        onBlur={handleBlur}
        onChange={handleChange}
      />
      <button
        className="text-center border cursor-pointer bg-white"
        style={{ width: "50px", height: "50px", lineHeight: "50px" }}
        onClick={handlePlus}
      >
        +
      </button>
    </div>
  );
};

export default InputNumber;
