import cls from "classnames";
import { HTMLProps } from "react";

interface InputTextProps extends HTMLProps<HTMLInputElement> {
  fullWidth?: boolean;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  error?: boolean;
}
const InputText: React.FC<InputTextProps> = ({
  fullWidth = false,
  placeholder,
  value,
  onChange,
  error,
  ...rest
}) => {

  return (
    <input
      className={cls(
        "border border-gray-300 p-2 my-2",
        { "w-full": fullWidth },
        {
          "border-red-400": error,
        }
      )}
      placeholder={placeholder}
      value={value}
      onChange={onChange}
      {...rest}
    />
  );
};

export default InputText;
