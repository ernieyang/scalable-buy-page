import { useEffect, useMemo } from "react";
import ReactDOM from "react-dom";

interface PortalProps {
  parent?: HTMLElement;
  className?: string;
}

const Portal: React.FC<PortalProps> = ({ children, parent, className }) => {
  const el = useMemo(() => document.createElement("div"), []);

  useEffect(() => {
    const target = parent && parent.appendChild ? parent : document.body;
    const classList: string[] = [];

    if (className) className.split(" ").forEach((item) => classList.push(item));
    classList.forEach((item) => el.classList.add(item));
    target.appendChild(el);
    return () => {
      target.removeChild(el);
    };
  }, [className, el, parent]);

  return ReactDOM.createPortal(children, el);
};

export default Portal;
