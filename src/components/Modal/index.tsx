import cls from "classnames";
import { useEffect, useLayoutEffect } from "react";
import Portal from "./Portal";

interface ModalProps {
  keepMounted?: boolean;
  open: boolean;
  container?: HTMLElement;
  onClose: () => void;
}

function getScrollbarSize(doc: Document): number {
  const scrollDiv = doc.createElement("div");
  scrollDiv.style.width = "99px";
  scrollDiv.style.height = "99px";
  scrollDiv.style.position = "absolute";
  scrollDiv.style.top = "-9999px";
  scrollDiv.style.overflow = "scroll";

  doc.body.appendChild(scrollDiv);
  const scrollbarSize = scrollDiv.offsetWidth - scrollDiv.clientWidth;
  doc.body.removeChild(scrollDiv);

  return scrollbarSize;
}

const Modal: React.FC<ModalProps> = ({
  keepMounted = false,
  open,
  children,
  container = document.body,
  onClose,
}) => {
  useLayoutEffect(() => {
    if (
      open &&
      document.body.clientHeight - window.innerHeight &&
      getScrollbarSize(container.ownerDocument)
    ) {
      container.style.overflow = "hidden";
      container.style.paddingRight = "15px";
    } else {
      container.style.overflow = "auto";
      container.style.paddingRight = "0";
    }
  }, [open, container]);

  if (!keepMounted && !open) {
    return null;
  }

  return (
    <Portal parent={container}>
      <div
        className={cls({
          hidden: !open,
        })}
      >
        <div
          className="fixed w-full h-full top-0 left-0 cursor-pointer"
          style={{ backgroundColor: "rgba(0,0,0, 0.25)" }}
          onClick={onClose}
        ></div>
        <div>{children}</div>
      </div>
    </Portal>
  );
};

export default Modal;
