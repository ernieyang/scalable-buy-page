import cls from "classnames";
import { useImgContext } from "./context";

const ProductImgPicker: React.FC = () => {
  const { currentImgIdx, setImgIdx, images } = useImgContext();

  return (
    <div>
      {images.map(({ id, url }, k) => (
        <div
          className={cls("p-0.5 mb-4 border", {
            " border-darkBlue-1": currentImgIdx === k,
          })}
          key={id}
          onClick={() => setImgIdx(k)}
        >
          <img src={url} />
        </div>
      ))}
    </div>
  );
};

export default ProductImgPicker;
