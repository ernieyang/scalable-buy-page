import { memo, createContext, useState, useContext, useEffect, useRef, ReactNode } from "react";
import { useCustomCompareMemo } from "use-custom-compare";
import { equals, findIndex, where } from "ramda";

const usePrevious = (value: any) => {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
};

interface ImgContextProviderStore {
  currentImgIdx: number;
  setImgIdx: (idx: number) => void;
  images: { id: string; url: string }[];
}

const ImgContext = createContext({} as ImgContextProviderStore);

interface ImgContextProviderProps {
  images: { id: string; url: string }[];
  children: ReactNode[];
}

const ImgContextProvider: React.FC<ImgContextProviderProps> = ({
  images,
  children,
}) => {

  const [imgIdx, setImgIdx] = useState(0);
  const imageRef = usePrevious(images[imgIdx]?.url);

  useEffect(() => {
    const imageIdx = findIndex(where({ url: equals(imageRef)   }))(images);
    setImgIdx(imageIdx !== -1 ? imageIdx: 0);
    
  }, [images]);


  const value = useCustomCompareMemo(()=> ({
    currentImgIdx: imgIdx,
    setImgIdx,
    images,
  }), [imgIdx, setImgIdx, images], equals)

  return (
    <ImgContext.Provider
      value={value}
    >
      {children}
    </ImgContext.Provider>
  );
};

export default memo(ImgContextProvider, (a,b) => {
  return equals(a['images'],b['images'])
});

ImgContextProvider.whyDidYouRender = true
export const useImgContext: () => ImgContextProviderStore = () =>
  useContext(ImgContext);
