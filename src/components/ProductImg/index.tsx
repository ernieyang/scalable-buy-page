import ProductImgPicker from "./Picker";
import { useImgContext } from "./context";

const ProductImg: React.FC = () => {
  const { currentImgIdx, images } = useImgContext();

  const url = images[currentImgIdx]?.url;

  if (!url) {
    return null;
  }

  return <img src={images[currentImgIdx].url} />;
};

export default ProductImg;

export { ProductImgPicker };
