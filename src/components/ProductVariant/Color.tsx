import cls from "classnames";
import { IColorVariant } from "./types";

interface ColorOptionsProps {
  options: IColorVariant["options"];
  selectValue: string;
  onChange: (value: string) => void;
}

const ColorOptions: React.FC<ColorOptionsProps> = ({
  options,
  selectValue,
  onChange,
}) => {
  return (
    <div className="flex">
      {options.map((o) => (
        <div
          className={cls(
            "p-0.5 border rounded-full mr-2 last:mr-0 cursor-pointer",
            {
              "border-darkBlue-1": selectValue === o.hex,
            },
            {
              "border-transparent": selectValue !== o.hex,
            },
            {
              "pointer-events-none border-red-700": o.enabled === false,
            }
          )}
          key={o.hex}
        >
          <div
            className="w-8 h-8 border border-0.5 rounded-full"
            style={{ backgroundColor: `#${o.hex}` }}
            onClick={() => {
              onChange(o.value);
            }}
          ></div>
        </div>
      ))}
    </div>
  );
};

export default ColorOptions;
