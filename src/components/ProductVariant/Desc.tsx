interface VariantDescProps {
  optionName: string;
  displayValueName?: string;
}

const VariantDesc: React.FC<VariantDescProps> = ({
  optionName,
  displayValueName,
}) => {
  return (
    <div className="font-bold  text-darkBlue-1 mb-3">
      {optionName.toUpperCase()}:
      <span className="ml-2 font-normal text-gray-400">
        {displayValueName || `Select a ${optionName.toLowerCase()}`}
      </span>
    </div>
  );
};

export default VariantDesc;
