import cls from "classnames";
import { ITextVariant } from "./types";

interface TextOptionsProps {
  options: ITextVariant["options"];
  selectValue: string;
  onChange: (value: string) => void;
}

const TextOptions: React.FC<TextOptionsProps> = ({
  options,
  selectValue,
  onChange,
}) => {
  return (
    <div className="flex">
      {options.map((o) => (
        <div
          key={o.value}
          className={cls(
            "mr-2 last:mr-0 p-3 border bg-white text-center cursor-pointer",
            {
              "border-darkBlue-1": selectValue === o.value,
            },
            { "border-gray-300": selectValue !== o.value },
            { "pointer-events-none text-red-700": o.enabled === false }
          )}
          style={{ minWidth: "50px" }}
          onClick={() => {
            onChange(o.value);
          }}
        >
          {o.displayName}
        </div>
      ))}
    </div>
  );
};

export default TextOptions;
