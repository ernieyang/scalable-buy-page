import cls from "classnames";
import { IThumbnailVariant } from "./types";

interface ThumbnailOptionsProps {
  options: IThumbnailVariant["options"];
  selectValue: string;
  onChange: (value: string) => void;
}

const ThumbnailOptions: React.FC<ThumbnailOptionsProps> = ({
  options,
  selectValue,
  onChange,
}) => {
  return (
    <div className="flex">
      {options.map((o) => (
        <div
          key={o.value}
          className={cls(
            "mr-2 last:mr-0 p-1 border cursor-pointer",
            {
              "border-darkBlue-1": selectValue === o.value,
            },
            { "border-gray-300": selectValue !== o.value }
          )}
          onClick={() => {
            onChange(o.value);
          }}
        >
          <img src={o.url} style={{ width: "70px" }} />
        </div>
      ))}
    </div>
  );
};

export default ThumbnailOptions;
