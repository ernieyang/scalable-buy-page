import VariantDesc from "./Desc";
import { IColorVariant, ITextVariant, IThumbnailVariant } from "./types";
import ColorOptions from "./Color";
import TextOptions from "./Text";
import ThumbnailOptions from "./Thumbnail";

export interface ProductVariantProps {
  variant: IColorVariant | ITextVariant | IThumbnailVariant;
  value: string;
  onChange: (object: { [key: string]: string }) => void;
}

const ProductVariant: React.FC<ProductVariantProps> = ({
  variant,
  value,
  onChange,
}) => {
  const { optionName } = variant;

  const displayName = variant.options.find((o) => o.value === value)
    ?.displayName;

  const renderOptions = (variant: ProductVariantProps["variant"]) => {
    if (variant.type === "color") {
      return (
        <ColorOptions
          options={variant.options}
          selectValue={value}
          onChange={(v) => onChange({ [variant.id]: v })}
        />
      );
    }
    if (variant.type === "text") {
      return (
        <TextOptions
          options={variant.options}
          selectValue={value}
          onChange={(v) => onChange({ [variant.id]: v })}
        />
      );
    }
    if (variant.type === "thumbnail") {
      return (
        <ThumbnailOptions
          options={variant.options}
          selectValue={value}
          onChange={(v) => onChange({ [variant.id]: v })}
        />
      );
    }
    return null;
  };

  return (
    <div>
      <VariantDesc optionName={optionName} displayValueName={displayName} />
      {renderOptions(variant)}
    </div>
  );
};

export default ProductVariant;
