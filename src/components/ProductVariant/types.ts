export interface IVariantOptions {
  id: string;
  type: string;
  optionName: string;
  options: {
    value: string;
    displayName: string;
    enabled: boolean;
  }[];
  disabled: string[];
}

export interface IColorVariant extends IVariantOptions {
  type: "color";
  options: (IVariantOptions["options"][0] & {
    hex: string;
  })[];
}

export interface ITextVariant extends IVariantOptions {
  type: "text";
}

export interface IThumbnailVariant extends IVariantOptions {
  type: "thumbnail";
  options: (IVariantOptions["options"][0] & {
    url: string;
  })[];
}
