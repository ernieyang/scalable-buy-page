import "./style.css";
import React from "react";
import ReactDOM from "react-dom";
import { RecoilRoot } from "recoil";
import whyDidYouRender from "@welldone-software/why-did-you-render";

//console.log(whyDidYouRender);
whyDidYouRender(React, {
  trackAllPureComponents: true,
});
import Main from "./main";

const App = () => (
  <RecoilRoot>
    <Main />
  </RecoilRoot>
);



ReactDOM.render(<App />, document.getElementById("root"));
