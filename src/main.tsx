import { useState, useEffect, useRef } from "react";
import { useRecoilState, useRecoilValue } from "recoil";
import { pageInfoState } from "./atoms/page";
import { productvariantState } from "./atoms/checkout";
import { selectedOptionState, quantityState } from "./atoms/checkout";
import {
  includes,
  pipe,
  reduce,
  map,
  uniq,
  filter,
  flatten,
  toPairs,
  where,
  assoc,
  flip,
  mergeAll,
} from "ramda";
import Button from "./components/Button";
import Collapse from "./components/Collapse";
import ProductImg, { ProductImgPicker } from "./components/ProductImg";
import ImgContextProvider from "./components/ProductImg/context";
import ProductVariant, {
  ProductVariantProps,
} from "./components/ProductVariant";
import InputNumber from "./components/InputNumber";

import Checkout from "./Checkout";

import { convertMoney } from "./utils";

import DarkLogo from "../public/Scalable_Logo_Dark.svg";

const mockFetch = () =>
  new Promise((resolve) => {
    const model = {
      secret: "123456",
      slug: "/test",
      isActive: true,
      product: {
        id: "colmi-m8",
        name:
          "COLMI P8 Plus 1.69 inch 2021 Smart Watch Men Full Touch Fitness Tracker IP67 waterproof Women GTS 2 Smartwatch for Xiaomi phone",
        desc:
          '\n  <div id="product-description" data-spm="1000023" class="product-description"><div class="origin-part box-sizing"><div class="detailmodule_media"><div class="video-container-297218770057 video-container-detail" exp_trigger="" projectid="180119" exp_page="detail_page" exp_page_area="" exp_type="item_description_broadcast_video" exp_condition="" exp_product="1005002032358980" style="width: 560px; height: 315px; position: relative; margin: auto; display: flex;" data-aplus-ae="x15_72f97416" data-spm-anchor-id="a2g0o.detail.1000023.i1.4410999d1ATVLS"><video width="560" height="315" controls="controls" poster="https://img.alicdn.com/imgextra/i2/6000000005387/O1CN01xPvmjk1pfIGPW9b0a_!!6000000005387-0-tbvideo.jpg" id="297218770057" src="https://cloud.video.taobao.com/play/u/1095216890732/p/1/e/6/t/10301/297218770057.mp4" style="background-color: rgb(34, 34, 34); display: none;"></video><span class="play-icon" clk_trigger="" projectid="180119" ae_page_type="detail_page" ae_button_type="item_description_broadcast_video" ae_object_type="product" ae_object_value="1005002032358980" data-aplus-clk="x6_4a236ff9" data-spm-anchor-id="a2g0o.detail.1000023.i0.4410999d1ATVLS"></span><img id="poster" src="https://img.alicdn.com/imgextra/i2/6000000005387/O1CN01xPvmjk1pfIGPW9b0a_!!6000000005387-0-tbvideo.jpg" style="max-width: 100%; max-height: 100%; display: block;"></div></div>\n<div class="detailmodule_text"><p class="detail-desc-decorate-content" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 300;font-size: 14px;line-height: 20px;color: #000;margin-bottom: 12px;">  </p></div>\n<div class="detailmodule_text"><p class="detail-desc-decorate-title" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 900;font-size: 20px;line-height: 28px;color: #000;margin-bottom: 12px;">COLMI P8 Plus</p><p class="detail-desc-decorate-content" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 300;font-size: 14px;line-height: 20px;color: #000;margin-bottom: 12px;" data-spm-anchor-id="a2g0o.detail.1000023.i2.4410999d1ATVLS">COLMI P8 Plus is our latest generation and most cost-effective smart watch. In this new version, COLMI P8 Plus smart watch has all-new 1.69" colour display and full capacitive touch, supporting taps and swipes, so it is easy to read and operate.\n\nSoft silicone strap makes COLMI P8 Plus lightweight and comfortable wear on your wrist and is available in 4 beautiful colours with matching swappable straps.\n </p></div>\n<div class="detailmodule_text"><p class="detail-desc-decorate-title" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 900;font-size: 20px;line-height: 28px;color: #000;margin-bottom: 12px;">COLMI P8 Plus vs. P8</p><p class="detail-desc-decorate-content" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 300;font-size: 14px;line-height: 20px;color: #000;margin-bottom: 12px;">P8 Plus upgrade:\nScreen: Upgrade from 1.4 inch to 1.69 inch\nScreen resolution: Upgrade from 240*240 resolution to 240*280 resolution.\nBattery: Upgrade from 170 mAh to 190 mAh\nWaterproof: Upgrade from IPX7 to IP67\nNew function: Rotate the button to operate the watch.\nMore memory, brand new UI interface.\n   </p></div>\n<div class="detailmodule_text"><p class="detail-desc-decorate-title" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 900;font-size: 20px;line-height: 28px;color: #000;margin-bottom: 12px;">Stay healthy</p><p class="detail-desc-decorate-content" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 300;font-size: 14px;line-height: 20px;color: #000;margin-bottom: 12px;">&gt; Sports: All-Day Activity Tracking, IP67 waterproof, 8 Exercise Modes, Stopwatch, Sports Data Report.\n\n&gt; Health: 24/7 Heart Rate, Blood Pressure, Blood Oxygen, Sleep Tracking &amp; Stages, Reminders to Move, Track your menstrual cycle.\n\n&gt; Life: Smartphone Notifications, Alarm clock, Weather, Shutter, Control music, 100+ watch faces. (support custom watch faces.)\n\nWith a battery life up to 7 days, keep the inspiration coming day and night.\n </p></div>\n<div class="detailmodule_text"><p class="detail-desc-decorate-title" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 900;font-size: 20px;line-height: 28px;color: #000;margin-bottom: 12px;">Language</p><p class="detail-desc-decorate-content" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 300;font-size: 14px;line-height: 20px;color: #000;margin-bottom: 12px;">Push information: Support all languages\n\nSmart watch languages: English, Russian, Spanish, Portuguese, Japanese, Korean, German, French, Arabic, Ukrainian, Italian, Indonesian, Thai, Chinese, Traditional Chinese\n\nAPP language: Russian, Spanish, Portuguese, German, Italian, Czech, Japanese, French, Polish, Thai, Swedish, Ukrainian, Finnish, English, Dutch, Vietnamese, Arabic, Korean, Danish, Bulgarian, Bokmal, Norwegian, Hindi, Indonesian, Pashto, Simplified Chinese, Traditional Chinese.\n </p></div>\n<div class="detailmodule_image"><img src="https://ae01.alicdn.com/kf/H3f95356212bd4db7aef8b9e21050e712c.jpg" class="detail-desc-decorate-image"><img src="https://ae04.alicdn.com/kf/H437c9de4dc98457da331d2d78e3f1e508.jpg" class="detail-desc-decorate-image"><img src="https://ae04.alicdn.com/kf/Hddcab267bf0b42cb817780f029515f87h.jpg" class="detail-desc-decorate-image"><img src="https://ae01.alicdn.com/kf/Hc7ade77a4e884431990c57629f625c8fE.jpg" class="detail-desc-decorate-image" style="margin-bottom: 10px;"><img src="https://ae04.alicdn.com/kf/Hf73fbec509544d08b8af38c023e4131dF.jpg" class="detail-desc-decorate-image"><img src="https://ae04.alicdn.com/kf/Hb821fb8612f541919ee836e1fa4733755.jpg" class="detail-desc-decorate-image"><img src="https://ae04.alicdn.com/kf/Hd857a4e26dfb47abb375279ff65fa1ebV.jpg" class="detail-desc-decorate-image"><img src="https://ae04.alicdn.com/kf/Ha9767b5c43824cb4a76139381401edbes.jpg" class="detail-desc-decorate-image"><img src="https://ae01.alicdn.com/kf/H66da98cecee442f79dc4bfcc29ae558b8.jpg" class="detail-desc-decorate-image"><img src="https://ae04.alicdn.com/kf/H6c4d05e08650498a95da2764f193066aj.jpg" class="detail-desc-decorate-image"></div>\n<div class="detailmodule_image"><img src="https://ae04.alicdn.com/kf/Hd4965116cea446318b016ac42aca70bc7.jpg" class="detail-desc-decorate-image"></div>\n<div class="detailmodule_text"><p class="detail-desc-decorate-content" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 300;font-size: 14px;line-height: 20px;color: #000;margin-bottom: 12px;"> </p></div>\n<div class="detailmodule_text-image"><p class="detail-desc-decorate-title" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 900;font-size: 20px;line-height: 28px;color: #000;margin-bottom: 12px;">Package Included</p><p class="detail-desc-decorate-content" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 300;font-size: 14px;line-height: 20px;color: #000;margin-bottom: 12px;">P8 Plus Smart watch * 1 \nUser manual * 1 \nUSB Cable * 1 </p><img src="https://ae01.alicdn.com/kf/H28349d08541a43c184c3e34377a26d31j.jpg" class="detail-desc-decorate-image"></div>\n<div class="detailmodule_text"><p class="detail-desc-decorate-content" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 300;font-size: 14px;line-height: 20px;color: #000;margin-bottom: 12px;">  </p></div>\n<div class="detailmodule_text"><p class="detail-desc-decorate-title" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 900;font-size: 20px;line-height: 28px;color: #000;margin-bottom: 12px;">COLMI official store</p><p class="detail-desc-decorate-content" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 300;font-size: 14px;line-height: 20px;color: #000;margin-bottom: 12px;">Authentic COLMI smart watch are engineered and manufactured with high-quality materials, and they\'re always quality-tested before they leave our factory. \nSince counterfeit or fake devices are not built or tested to these standards, they might perform poorly or malfunction. So, we recommend buying devices sold only by our official store.\n  </p></div>\n<div class="detailmodule_text"><p class="detail-desc-decorate-title" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 900;font-size: 20px;line-height: 28px;color: #000;margin-bottom: 12px;">In stock</p><p class="detail-desc-decorate-content" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 300;font-size: 14px;line-height: 20px;color: #000;margin-bottom: 12px;">The COLMI P8 Plus smartwatch is in stock.\n </p></div>\n<div class="detailmodule_text-image"><p class="detail-desc-decorate-title" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 900;font-size: 20px;line-height: 28px;color: #000;margin-bottom: 12px;">Photos</p><img src="https://ae01.alicdn.com/kf/H9dbb5617d7f240f98eabfedf976b1a296.jpg" class="detail-desc-decorate-image" style="margin-bottom: 10px;"><img src="https://ae01.alicdn.com/kf/H1abfa34a33b5474fb9544454603da7f7c.jpg" class="detail-desc-decorate-image" style="margin-bottom: 10px;"><img src="https://ae01.alicdn.com/kf/Hcfe8ca551fef476db6e13c6a8ea4117el.jpg" class="detail-desc-decorate-image" style="margin-bottom: 10px;"><img src="https://ae01.alicdn.com/kf/H950bf94a3546436587b50dfdd5fc8258Q.jpg" class="detail-desc-decorate-image" style="margin-bottom: 10px;"><img src="https://ae01.alicdn.com/kf/H685a19c7c37a451cb8662624dd803f0ar.jpg" class="detail-desc-decorate-image" style="margin-bottom: 10px;"><img src="https://ae01.alicdn.com/kf/H056885daacb240489607537fca98c792G.jpg" class="detail-desc-decorate-image" style="margin-bottom: 10px;"><img src="https://ae01.alicdn.com/kf/H8201970fb3b243108b5c11816196225bH.jpg" class="detail-desc-decorate-image" style="margin-bottom: 10px;"></div>\n<div class="detailmodule_text"><p class="detail-desc-decorate-title" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 900;font-size: 20px;line-height: 28px;color: #000;margin-bottom: 12px;">  </p></div>\n<div class="detailmodule_text"><p class="detail-desc-decorate-title" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 900;font-size: 20px;line-height: 28px;color: #000;margin-bottom: 12px;">Warranty Policy</p><p class="detail-desc-decorate-content" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 300;font-size: 14px;line-height: 20px;color: #000;margin-bottom: 12px;">Warranty one year.</p></div>\n<div class="detailmodule_text-image"><img src="//ae01.alicdn.com/kf/H9ab7af3818ce4a278c5589a99e6ca66ad.jpg" class="detail-desc-decorate-image"></div>\n<div class="detailmodule_text"><p class="detail-desc-decorate-content" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 300;font-size: 14px;line-height: 20px;color: #000;margin-bottom: 12px;">  </p></div>\n<div class="detailmodule_text-image"><p class="detail-desc-decorate-title" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 900;font-size: 20px;line-height: 28px;color: #000;margin-bottom: 12px;">Back to Home</p><a href="https://colmi.aliexpress.com/store/615486"><img src="//ae01.alicdn.com/kf/Ha8c81cd5313b4fea8a66209c66b7b0b26.jpg" class="detail-desc-decorate-image" style="margin-bottom: 10px;"></a></div>\n<div class="detailmodule_text"><p class="detail-desc-decorate-title" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 900;font-size: 20px;line-height: 28px;color: #000;margin-bottom: 12px;">Brand Story</p><p class="detail-desc-decorate-content" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 300;font-size: 14px;line-height: 20px;color: #000;margin-bottom: 12px;">COLMI was established in 2012.\n\nSlogan: Young and simple, but never naive.\n\nMission: Provide cost-effective smart electronics.</p></div>\n<div class="detailmodule_text"><p class="detail-desc-decorate-content" style="text-overflow: ellipsis;font-family: \'OpenSans\';color:\'#000\';word-wrap: break-word;white-space: pre-wrap;font-weight: 300;font-size: 14px;line-height: 20px;color: #000;margin-bottom: 12px;">    </p></div>\n\n</div></div>',
        imageGalleries: [
          {
            id: "image-1",
            url:
              "https://ae01.alicdn.com/kf/Hbe41c06d0a5045dc9d314b38427cf25b2/COLMI-P8-Plus-1-69-inch-2021-Smart-Watch-Men-Full-Touch-Fitness-Tracker-IP67-waterproof.jpg_Q90.jpg_.webp",
          },
          {
            id: "image-2",
            url:
              "https://ae01.alicdn.com/kf/H9d3b132477844c1b9298a89d6350c533A/COLMI-P8-Plus-1-69-inch-2021-Smart-Watch-Men-Full-Touch-Fitness-Tracker-IP67-waterproof.jpg_Q90.jpg_.webp",
          },
          {
            id: "image-3",
            url:
              "https://ae01.alicdn.com/kf/H85bc17fb8b6645a2a921df5b7a80bce2C/COLMI-P8-Plus-1-69-inch-2021-Smart-Watch-Men-Full-Touch-Fitness-Tracker-IP67-waterproof.jpg_Q90.jpg_.webp",
          },
          {
            id: "image-4",
            url:
              "https://ae01.alicdn.com/kf/Ha908c604fdf54af8b2954bc23ce4c73eN/COLMI-P8-Plus-1-69-inch-2021-Smart-Watch-Men-Full-Touch-Fitness-Tracker-IP67-waterproof.jpg_Q90.jpg_.webp",
          },
          {
            id: "image-5",
            url:
              "https://ae01.alicdn.com/kf/H6ba9249aac7f454aa9f9cdd910321994d/COLMI-P8-Plus-1-69-inch-2021-Smart-Watch-Men-Full-Touch-Fitness-Tracker-IP67-waterproof.jpg_Q90.jpg_.webp",
          },
        ],
        variantOptions: [
          {
            id: "option-1",
            type: "color",
            optionName: "Color",
            options: [
              {
                value: "111111",
                displayName: "Black",
                hex: "111111",
              },
              {
                value: "ffffff",
                displayName: "White",
                hex: "ffffff",
              },
            ],
          },
          {
            id: "option-2",
            type: "text",
            optionName: "Size",
            options: [
              {
                value: "S",
                displayName: "S",
              },
              {
                value: "M",
                displayName: "M",
              },
              {
                value: "L",
                displayName: "L",
              },
              {
                value: "XL",
                displayName: "XXXXL",
              },
            ],
          },
          {
            id: "option-3",
            type: "thumbnail",
            optionName: "Option",
            options: [
              {
                value: "ss",
                displayName: "Rabbit",
                url:
                  "https://ae01.alicdn.com/kf/Hbe41c06d0a5045dc9d314b38427cf25b2/COLMI-P8-Plus-1-69-inch-2021-Smart-Watch-Men-Full-Touch-Fitness-Tracker-IP67-waterproof.jpg_Q90.jpg_.webp",
              },
            ],
          },
        ],
        variants: [
          {
            id: "colmi-m8-1",
            selectedOptions: [
              {
                id: "option-1",
                value: "111111",
              },
              {
                id: "option-2",
                value: "M",
              },
              {
                id: "option-3",
                value: "ss",
              },
            ],
            price: 999,
            inStock: true,
            imageGalleryIds: [
              "image-1",
              "image-2",
              "image-3",
              "image-4",
              "image-5",
            ],
          },
          {
            id: "colmi-m8-1",
            selectedOptions: [
              {
                id: "option-1",
                value: "111111",
              },
              {
                id: "option-2",
                value: "L",
              },
              {
                id: "option-3",
                value: "ss",
              },
            ],
            price: 999,
            inStock: true,
            imageGalleryIds: [
              "image-1",
              "image-2",
              "image-3",
              "image-4",
              "image-5",
            ],
          },
          {
            id: "colmi-m8-1",
            selectedOptions: [
              {
                id: "option-1",
                value: "111111",
              },
              {
                id: "option-2",
                value: "XL",
              },
              {
                id: "option-3",
                value: "ss",
              },
            ],
            price: 999,
            inStock: true,
            imageGalleryIds: [
              "image-1",
              "image-2",
              "image-3",
              "image-4",
              "image-5",
            ],
          },
          {
            id: "colmi-m8-1",
            selectedOptions: [
              {
                id: "option-1",
                value: "ffffff",
              },
              {
                id: "option-2",
                value: "S",
              },
              {
                id: "option-3",
                value: "ss",
              },
            ],
            price: 1,
            inStock: true,
            imageGalleryIds: ["image-2", "image-3", "image-4", "image-5"],
          },
          {
            id: "colmi-m8-1",
            selectedOptions: [
              {
                id: "option-1",
                value: "ffffff",
              },
              {
                id: "option-2",
                value: "M",
              },
              {
                id: "option-3",
                value: "ss",
              },
            ],
            price: 2,
            inStock: true,
            imageGalleryIds: ["image-3", "image-4", "image-5"],
          },
          {
            id: "colmi-m8-1",
            selectedOptions: [
              {
                id: "option-1",
                value: "ffffff",
              },
              {
                id: "option-2",
                value: "XL",
              },
              {
                id: "option-3",
                value: "ss",
              },
            ],
            price: 4,
            inStock: false,
            imageGalleryIds: ["image-5"],
          },
        ],
      },
    };
    setTimeout(() => {
      resolve(model);
    }, 1000);
  });

const Main: React.FC = () => {
  const [open, setOpen] = useState(false);
  const lastChangeRef = useRef<{ id?: string; value?: string }>({});
  const [quantity, setQuantity] = useRecoilState(quantityState);
  const productVariant = useRecoilValue(productvariantState);
  const [imgs, setImgs] = useState<{ id: string; url: string }[]>([]);
  const [page, setPage] = useRecoilState(pageInfoState);
  const [selectedOptions, setSelectedOptions] = useRecoilState(
    selectedOptionState
  );
  const [variantOptions, setVariantOptions] = useState<any[]>([]);

  const { variants = [] } = page?.product || {};

  const test = () => {
    const availableSelectOptions = pipe(
      map((v) => v.selectedOptions),
      filter(includes(lastChangeRef.current)),
      flatten,
      uniq,
      reduce((a, b) => {
        return { ...a, [b.id]: [...(a[b.id] || []), b.value] };
      }, {})
    )(variants);

    const availableVariantOptions = variantOptions.map((vo) => ({
      ...vo,
      options: vo.options.map((o) => {
        if (vo.id === lastChangeRef.current.id) {
          return o;
        }
        return {
          ...o,
          enabled: availableSelectOptions[vo.id].includes(o.value),
        };
      }),
    }));

    setVariantOptions(availableVariantOptions);
  };

  const handleOptionChange = (option: { [key: string]: string }) => {
    lastChangeRef.current = pipe(
      toPairs,
      map((x) => ({ id: x[0], value: x[1] })),
      mergeAll
    )(option);

    setSelectedOptions((s) => ({
      ...s,
      ...option,
    }));
    test();
  };

  useEffect(() => {
    (async () => {
      const model = await mockFetch();
      setPage(model);
      setVariantOptions(
        map((x) => ({
          ...x,
          options: map(assoc("enabled", true))(x.options),
        }))(model.product.variantOptions)
      );
    })();
  }, [setPage]);

  useEffect(() => {
    if (
      !Object.keys(selectedOptions).length &&
      variantOptions.length &&
      variants.length
    ) {
      const selectedOptions = mergeAll(
        variants[0].selectedOptions.map((so) => ({ [so.id]: so.value }))
      );
      setSelectedOptions(selectedOptions);
      lastChangeRef.current = {
        id: variantOptions[0].id,
        value: selectedOptions[variantOptions[0].id],
      };
      test();
    }
  }, [selectedOptions, setSelectedOptions, variantOptions, variants]);

  useEffect(() => {
    let filteredVariants = variants;
    const x = filter(
      where({ selectedOptions: includes({ id: "option-1", value: "111111" }) })
    )(variants);
    for (const [key, value] of Object.entries(selectedOptions)) {
      filteredVariants = filter(
        where({ selectedOptions: includes({ id: key, value }) })
      )(filteredVariants);
    }
    if (filteredVariants.length) {
      const defaultVaraint = filteredVariants[0];
      const images = filter(
        where({
          id: flip(includes)(defaultVaraint.imageGalleryIds),
        })
      )(page.product.imageGalleries);
      setImgs(images);
    }
  }, [selectedOptions]);

  if (!page) {
    return null;
  }

  return (
    <div className="flex flex-col min-h-screen bg-lightGray-1">
      <div
        className="flex-1 mt-16 mb-24 mx-auto"
        style={{ maxWidth: "1200px", width: "100%" }}
      >
        <div className="grid grid-cols-12 gap-6">
          <ImgContextProvider images={imgs}>
            <div className="col-span-1">
              <ProductImgPicker />
            </div>
            <div className="col-span-6">
              <ProductImg />
            </div>
          </ImgContextProvider>
          <div className="col-span-5 row-span-1 sticky top-0">
            <div className="mx-10">
              <h1 className="text-2xl text-darkBlue-1 mt-9 mb-3">
                {page?.product?.name || ""}
              </h1>
              <div className="font-bold text-xl text-darkBlue-1 mb-3">
                ${convertMoney(productVariant.price)}
              </div>
              <div className="my-8">
                <div>
                  {variantOptions.map((PV, k) => (
                    <div key={k} className="mb-4">
                      <ProductVariant
                        variant={PV as ProductVariantProps["variant"]}
                        value={selectedOptions[PV.id] || ""}
                        onChange={handleOptionChange}
                      />
                    </div>
                  ))}
                </div>

                <div className="font-bold  text-darkBlue-1 mb-3">QUANTITY:</div>
                <div>
                  <InputNumber
                    max={999}
                    min={0}
                    value={quantity}
                    onChange={(value) => setQuantity(value)}
                  />
                </div>
              </div>
              <div className="mt-2">
                <Button onClick={() => setOpen(true)}>Buy Now</Button>
                <div className="text-darkGray-1 text-center mt-4">
                  Secure transaction
                </div>
              </div>
            </div>
          </div>
          <div className="col-span-7 row-span-1">
            <Collapse title="Details">
              <div
                dangerouslySetInnerHTML={{ __html: page.product.desc }}
              ></div>
            </Collapse>
          </div>
        </div>
      </div>
      <div className="bg-lightGray-2 text-darkGray-1 text-sm py-3.5 flex justify-center">
        Powered by <img className="px-2" src={DarkLogo} />
      </div>
      <Checkout open={open} onClose={() => setOpen(false)} />
    </div>
  );
};

export default Main;
