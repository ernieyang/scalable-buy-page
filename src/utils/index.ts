export const convertMoney = (amount: number, toFixed = 2): string =>
  (amount / 100).toFixed(toFixed);
