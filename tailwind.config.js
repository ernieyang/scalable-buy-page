module.exports = {
  purge: ["./src/**/*.html", "./src/**/*.tsx"],
  theme: {
    fontFamily: {
      sans: "proxima-nova, Georgia, sans-serif",
    },
    extend: {
      colors: {
        lightGray: {
          1: "#F7F9FB",
          2: "#EBF0F4",
        },
        darkGray: {
          1: "#4B535A",
        },
        darkBlue: {
          1: "#0E1E2D",
        },
      },
    },
  },
  variants: {
    extend: {
      margin: ["last"],
    },
  },
};
